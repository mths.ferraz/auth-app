# AuthApp

This project is a skeleton of an Angular 7 app using Firebase auth. It has an auth module that is guarded from users already authenticated and a home module guarded from unauthenticated users. The auth module has 3 pages: login, signup and recover password. For now, it only supports email and password signup.

Check the app [here](https://auth-app-303b8.firebaseapp.com/)

## Using on your own project

In order to build your app on top of the skeleton follow these steps:

1. Clone the repo creating a folder with your project name, and change to it

```
git clone git@gitlab.com:mths.ferraz/auth-app.git <YOUR PROJECT NAME>
cd <YOUR PROJECT NAME>
```

2. Set the current remote to be the `auth-skeleton-origin` and add your own

```
git remote rename origin auth-skeleton-origin
git remote add origin <YOUR REMOTE URL>
git push -u origin master
```

3. Create a firebase project and click on the _Add Firebase to your Web app_ button (if you already have an app deployed, this will be on the settings page):

   ![alt text](./readme-assets/create-web-app.png 'Add Firebase to your Web app button')

After clicking, you must see a code like this:

```html
<script src="https://www.gstatic.com/firebasejs/5.5.2/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: '<API_KEY>',
    authDomain: '<PROJECT_ID>.firebaseapp.com',
    databaseURL: 'https://<DATABASE_NAME>.firebaseio.com',
    projectId: '<PROJECT_ID>',
    storageBucket: '<BUCKET>.appspot.com',
    messagingSenderId: '<SENDER_ID>'
  };
  firebase.initializeApp(config);
</script>
```

Copy the value of the config object and replace the `firebase` field value of the `enviroment` variables on `src/environments/environment.ts` and `src/environments/environment.prod.ts` like so:

```javascript
export const environment = {
  //...
  firebase: {
    apiKey: '<API_KEY>',
    authDomain: '<PROJECT_ID>.firebaseapp.com',
    databaseURL: 'https://<DATABASE_NAME>.firebaseio.com',
    projectId: '<PROJECT_ID>',
    storageBucket: '<BUCKET>.appspot.com',
    messagingSenderId: '<SENDER_ID>'
  }
};
```

4. Change the default project on `.firebaserc` to your `<PROJECT_ID>`

```json
{
  "projects": {
    "default": "<PROJECT_ID>"
  }
}
```

5. Install the dependencies

```
npm i
```

6. Your app should now run with `npm start` and may be deployed with `ng build && firebase deploy --only hosting`. You can change the `LICENSE`, the `README.md`, the `package.json` and customize your app.

## Get updates from original auth-app repo

After the steps above, your master branch is now following your remote (that we called origin). If you want to pull the last updates of the original auth-repo you have to specify the remote:

```
git fetch auth-skeleton-origin
git checkout master
git merge auth-skeleton-origin/master
```

Be careful, tough, as it may contain changes to files that you've already customized (e.g. README, package.json)
