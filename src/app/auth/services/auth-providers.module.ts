import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { MatSnackBarModule } from '@angular/material';
import { UnauthGuard } from './unauth.guard';

@NgModule({
  imports: [
    CommonModule,
    AngularFireAuthModule,
    AngularFirestoreModule,
    MatSnackBarModule
  ],
  providers: [AuthService, AuthGuard, UnauthGuard],
  declarations: []
})
export class AuthProvidersModule {}
