import { AuthProvidersModule } from './auth-providers.module';

describe('AuthProvidersModule', () => {
  let authProvidersModule: AuthProvidersModule;

  beforeEach(() => {
    authProvidersModule = new AuthProvidersModule();
  });

  it('should create an instance', () => {
    expect(authProvidersModule).toBeTruthy();
  });
});
