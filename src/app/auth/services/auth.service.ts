import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material';
import { User } from './user';

@Injectable()
export class AuthService {
  user: Observable<User>;
  credentials: Observable<firebase.User>;

  constructor(
    private fireAuth: AngularFireAuth,
    private firestore: AngularFirestore,
    private snackbar: MatSnackBar
  ) {
    //// Get auth data, then get firestore user document || null
    this.credentials = this.fireAuth.authState;
    this.user = this.credentials.pipe(
      switchMap(user => {
        if (user) {
          return this.firestore.doc<User>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }

  async signup(email: string, password: string): Promise<boolean> {
    try {
      const credentials = await this.fireAuth.auth.createUserWithEmailAndPassword(
        email,
        password
      );
      await this.firestore.doc(`users/${credentials.user.uid}`).set({
        email: email
      });
      return true;
    } catch (error) {
      this.snackbar.open(
        `There was an error registering the user: ${error.message ||
          error.code ||
          error}.`,
        'Dismiss'
      );
      return false;
    }
  }
  async login(email: string, password: string): Promise<boolean> {
    try {
      await this.fireAuth.auth.signInWithEmailAndPassword(email, password);
      return true;
    } catch (error) {
      this.snackbar.open(
        `There was an error registering the user: ${error.message ||
          error.code ||
          error}.`,
        'Dismiss'
      );
      return false;
    }
  }
  async logout(): Promise<boolean> {
    try {
      await this.fireAuth.auth.signOut();
      return true;
    } catch (e) {
      return false;
    }
  }
  async sendPasswordResetEmail(email?: string) {
    try {
      if (!email) {
        email = this.fireAuth.auth.currentUser.email;
      }
      await this.fireAuth.auth.sendPasswordResetEmail(email);
      return true;
    } catch (error) {
      this.snackbar.open(
        `Something went wrong: ${error.message || error.code || error}`,
        'Dismiss'
      );
      return false;
    }
  }

  async updateEmail(newEmail: string) {
    try {
      let currentUser = this.fireAuth.auth.currentUser;
      await currentUser.updateEmail(newEmail);
      await this.firestore.doc(`users/${currentUser.uid}`).update({
        email: newEmail
      });
      return true;
    } catch (error) {
      this.snackbar.open(
        `Something went wrong: ${error.message || error.code || error}`,
        'Dismiss'
      );
      return false;
    }
  }
}
