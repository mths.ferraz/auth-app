import { Injectable } from '@angular/core';
import {
  CanLoad,
  Route,
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { take, map, tap } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class UnauthGuard implements CanLoad, CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private snackbar: MatSnackBar
  ) {}

  test() {
    return this.authService.user.pipe(
      take(1),
      map(user => !user),
      tap(loggedOut => {
        if (!loggedOut) {
          const sb = this.snackbar.open('You are already logged in', 'Logout', {
            duration: 5000
          });
          sb.onAction().subscribe(async () => {
            try {
              await this.authService.logout();
              this.router.navigate(['/auth/login']);
            } catch (error) {
              console.error('could not logout');
            }
          });
          console.log('Access denied: already logged in');
          this.router.navigate(['/home']);
        }
      })
    );
  }

  canLoad(route: Route): Observable<boolean> {
    return this.test();
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.test();
  }
}
