import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  ValidationErrors
} from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Requirement } from 'src/app/core/requirements/requirements.component';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {
  form: FormGroup;
  isSent: boolean = false;
  isLoading: boolean = false;
  constructor(formBuilder: FormBuilder, private authService: AuthService) {
    this.form = formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  get email(): AbstractControl {
    return this.form.get('email');
  }

  emailRequirements: Requirement[] = [
    {
      message: 'Check the email format',
      fulfilled: () =>
        !(this.email.errors && this.email.errors.email) && this.email.value
    }
  ];

  ngOnInit() {}

  async submit() {
    if (!this.form.valid) {
      console.log('Form is not valid');
      return;
    }
    this.isLoading = true;
    const success = await this.authService.sendPasswordResetEmail(
      this.email.value
    );
    if (success) {
      this.isSent = true;
    }
    this.isLoading = false;
  }
}
