import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Observable } from 'rxjs';
import { User } from 'src/app/auth/services/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  sendingPassword: boolean = false;
  editingEmail: boolean = false;
  passwordSent: boolean = false;
  constructor(public authService: AuthService, private router: Router) {}

  ngOnInit() {}

  logout() {
    this.authService.logout();
    this.router.navigate(['/auth/login']);
  }

  toggleEmailEditing() {
    this.editingEmail = !this.editingEmail;
  }
  saveEmail(event: any) {
    this.editingEmail = false;
    this.authService.updateEmail(event.target.email.value);
  }

  async resetPassword() {
    this.sendingPassword = true;
    this.passwordSent = await this.authService.sendPasswordResetEmail();
    this.sendingPassword = false;
  }
}
