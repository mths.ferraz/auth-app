import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequirementsComponent } from './requirements.component';

@NgModule({
  imports: [CommonModule],
  declarations: [RequirementsComponent],
  exports: [RequirementsComponent]
})
export class RequirementsModule {}
