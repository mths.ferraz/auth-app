import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material';
import { PageNotFoundComponent } from './page-not-found.component';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

const routes: Routes = [{ path: '', component: PageNotFoundComponent }];
@NgModule({
  imports: [MatButtonModule, CommonModule, RouterModule.forChild(routes)],
  declarations: [PageNotFoundComponent]
})
export class PageNotFoundModule {}
