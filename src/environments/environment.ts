// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //TODO replace with your project parameters, also check .firebaserc
  firebase: {
    apiKey: 'AIzaSyA25CEISkY8coZJP6KUoJwFoDfETVRo_qk',
    authDomain: 'auth-app-303b8.firebaseapp.com',
    databaseURL: 'https://auth-app-303b8.firebaseio.com',
    projectId: 'auth-app-303b8',
    storageBucket: 'auth-app-303b8.appspot.com',
    messagingSenderId: '409305734442'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
